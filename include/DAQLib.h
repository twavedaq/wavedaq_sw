#include <string>
#include <string.h>
#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>
#include <atomic>
#include <stack>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

template <class T> class DAQMemoryPool;
class DAQBufferBase;
template <class T> class DAQBuffer;
class DAQAlarm;
class DAQThread;
class DAQServerThread;
class DAQSystem;

#ifndef DAQLIB_H
#define DAQLIB_H
// --- DAQ Alarm --- Thread safe alarm system
// currently limited to 32 alarms
// with callbacks
class DAQAlarm {
   typedef void (*callback_t)(unsigned int id, const std::string &description);

   std::vector<bool> fAlarmTriggered;
   std::vector<callback_t> fAlarmCallback;
   std::vector<std::string> fAlarmDescription;
   std::mutex fAccessMutex;

public:
   // no mutex lock
   bool Test (unsigned int id) const;
   // Access mutex lock
   void Trigger(unsigned int id);
   void Trigger(unsigned int id, const std::string &description);
   void Reset(unsigned int id);
   void Clean();
   std::string GetDescription(unsigned int id);

   //set callback for given alarm
   //the function is called in the thread generating the alarm
   void SetCallback(unsigned int id, callback_t callback);

   //Resize Alarm vector
   //Not thread safe
   void Resize(unsigned int size);

   DAQAlarm(unsigned int size = 32){
      Resize(size);
   }
};

// --- DAQ Memory Pool --- Template memory pool singleton implementation
// Usage by overriding new/delete operators in class:
// Pay Attantion with derived classes
// class myclass {
//
//   void* operator new(size_t size){
//      auto &mp = DAQMemoryPool<myclass>::GetInstance();
//      return mp.Allocate();
//   }
//
//   void operator delete(void* ptr){
//      auto &mp = DAQMemoryPool<myclass>::GetInstance();
//      mp.Deallocate(ptr);
//   }
// };
//
template <class T> class DAQMemoryPool{
   private:
      // c++17: avoid the additional singleton requirements by inlining the satic pointer here:
      //inline static DAQMemoryPool<T> *gMemPool = nullptr;
      std::atomic<int> fNElements;
      std::mutex fAllocMutex;
      std::stack<void*> fPool;

      //reserved Methods

      //singleton constructor
      DAQMemoryPool(){
         fNElements = 0;
         //printf("mempool created\n"); 
      }
      DAQMemoryPool(const DAQMemoryPool&)= delete;
      DAQMemoryPool& operator=(const DAQMemoryPool&)= delete;

      //allocate one additional element
      void *AllocateElement(){
         fNElements++;
         return ::operator new(sizeof(T));
      }

   public:
      //Methods
      static DAQMemoryPool<T>& GetInstance() {
         static DAQMemoryPool<T> gMemPool;

         return gMemPool;
      }

      //preallocate elements
      void PreAllocate(size_t n){
         std::lock_guard<std::mutex> guard(fAllocMutex);
         int curSize = fPool.size();
         for(int i=curSize; i<n; i++){
            fPool.push(AllocateElement());
         }
      }

      //remove all elements
      void Clean(){
         std::lock_guard<std::mutex> guard(fAllocMutex);
         while(!fPool.empty()){
            void* p = fPool.top();
            ::operator delete(p);
            fPool.pop();
         }
         fNElements = 0;
      }

      //get a pointer
      void* Allocate(){
         std::unique_lock<std::mutex> guard(fAllocMutex);
         void * p;
         if(!fPool.empty()){
            //from the pool
            p = fPool.top();
            fPool.pop();
         } else {
            //from outside the pool
            //printf("allocated element %d of type %s\n", (int)fNElements, typeid(T).name());
            guard.unlock();
            p = AllocateElement();
         }
         return p;
      }

      //return a pointer to the pool
      void Deallocate(void* p){
         std::lock_guard<std::mutex> guard(fAllocMutex);
         fPool.push(p);
      }
};

// --- DAQ Buffer Base --- virtual class for buffer interface functions
class DAQBufferBase {
   private:
      std::string fName;
   public:
      std::string  GetName(){ return fName; }

      virtual unsigned int GetSize() = 0;
      virtual void Clean() = 0;
      virtual unsigned int GetMaxSize() = 0;
      virtual float GetOccupancy() = 0;

      DAQBufferBase(DAQSystem* parent, std::string name);
      virtual ~DAQBufferBase(){};
};


// --- DAQ Buffer --- thread safe queue with max size
template <class T> class DAQBuffer : public DAQBufferBase {
   private:
      std::queue<T*> fEvents;
      unsigned int fMaxSize;
      std::mutex fAccess;
      std::condition_variable fHasData;
      std::chrono::microseconds fLockWaitDuration;

      //reserved Methods

   public:
      //Methods
      bool Try_push(T* data){
         std::lock_guard<std::mutex> lock(fAccess);
         //check size
         if(fEvents.size() < fMaxSize){
            //not full
            fEvents.push(data);
            fHasData.notify_one();
            return true;
         } else {
            //full
            //TODO: add exception
            //printf("BUFFER OVERSIZE");
            return false;
         }
      };
      // pops outs one event if available
      bool Try_pop(T* &ptr){
         std::unique_lock<std::mutex> lock(fAccess);
         //check size
         if(fEvents.size() == 0){
            //no data, wait
            /*std::cv_status status = fHasData.wait_for(lock,std::chrono::milliseconds(100), GetSize());
            if(status == std::cv_status::timeout) {
               lock.unlock();
               return false;
            }*/

            if(fHasData.wait_for(lock, fLockWaitDuration, [&]{return fEvents.size()!=0; }))
            {
               ptr = fEvents.front();
               fEvents.pop();
               lock.unlock();
               return true;
            } else {
               lock.unlock();
               return false;

            }
         }
         ptr = fEvents.front();
         fEvents.pop();
         lock.unlock();
         return true;
      }
      //push a bunch of events at once
      bool Try_push(std::vector<T*>& dataVector){
         std::lock_guard<std::mutex> lock(fAccess);
         bool success = true;
         for(auto &data : dataVector){
            //check size
            if(fEvents.size() < fMaxSize){
               //not full
               fEvents.push(data);
               data = nullptr;
            } else {
               //full
               //TODO: add exception
               //printf("BUFFER OVERSIZE");
               success = false;
               //could return here
            }
         }

         if(fEvents.size() > 0) 
            fHasData.notify_one();

         return success;
      };
      //push a bunch of events at once
      bool Try_push(std::vector<T*>& dataVector, std::vector<bool>& enableVector){
         if(dataVector.size() != enableVector.size()){
            printf("size mismatch %lu %lu\n", dataVector.size(), enableVector.size());
            return false;
         }

         std::lock_guard<std::mutex> lock(fAccess);

         bool success = true;
         for(unsigned int iData = 0; iData < dataVector.size(); iData++){
            // only for enabled items
            if(enableVector[iData]){
               //check size
               if(fEvents.size() < fMaxSize){
                  //not full
                  fEvents.push(dataVector[iData]);
                  dataVector[iData] = nullptr;
               } else {
                  //full
                  //TODO: add exception
                  //printf("BUFFER OVERSIZE");
                  success = false;
                  //could return here
               }
            }
         }

         if(fEvents.size() > 0) 
            fHasData.notify_one();

         return success;
      };
      // pops a bunch of events if available
      bool Try_pop(std::vector<T*> &ptr){
         std::unique_lock<std::mutex> lock(fAccess);
         //check size
         if(fEvents.size() == 0){
            //no data, wait
            /*std::cv_status status = fHasData.wait_for(lock,std::chrono::milliseconds(100), GetSize());
            if(status == std::cv_status::timeout) {
               lock.unlock();
               return false;
            }*/

            if(fHasData.wait_for(lock, fLockWaitDuration, [&]{return fEvents.size()!=0; }))
            {
               //got some data
            } else {
               lock.unlock();
               return false;

            }
         }
         unsigned long size = fEvents.size();
         if(ptr.capacity() < size){
            ptr.reserve(size);
         }
         for(unsigned long i=0; i<size; i++){
            ptr.push_back(fEvents.front());
            fEvents.pop();
         }
         lock.unlock();
         return true;
      }

      unsigned int GetSize(){
         std::lock_guard<std::mutex> lock(fAccess);

         return fEvents.size();
      }
      void Clean(){
         std::lock_guard<std::mutex> lock(fAccess);

         while(fEvents.size()){
            delete fEvents.front();
            fEvents.pop();
         }

      }

      //Setters
      void SetLockWaitDuration(std::chrono::microseconds d){ fLockWaitDuration = d; }

      //Getters
      unsigned int GetMaxSize(){ return fMaxSize; }
      float GetOccupancy(){ return fEvents.size() *1./fMaxSize; }//NOTE: only for monitoring

      //Constructor  
      DAQBuffer(unsigned int maxsize = 0, std::string name = "NEWBUFFER", DAQSystem* parent = nullptr): DAQBufferBase(parent, name){ 
         fMaxSize = maxsize;
         fLockWaitDuration = std::chrono::microseconds(100);
      }

      //Destructor
      ~DAQBuffer(){
      }

};

// --- DAQ Fanout Buffer --- Buffer with one input and multiple outputs
template <class T> class DAQFanoutBuffer : public DAQBufferBase {
private:
   std::vector<DAQBuffer<T>*> buffers;

public:
   //Methods
   //directly into buffer
   bool Try_push_into(T* data, unsigned int buffer){
      return buffers[buffer]->Try_push(data);
   }
   bool Try_pop_from(T* &ptr, unsigned int buffer){
      return buffers[buffer]->Try_pop(ptr);
   }
   bool Try_push_into(std::vector<T*> &data, unsigned int buffer){
      return buffers[buffer]->Try_push(data);
   }
   bool Try_pop_from(std::vector<T*> &ptr, unsigned int buffer){
      return buffers[buffer]->Try_pop(ptr);
   }
   //using a key
   bool Try_push(T* data, unsigned int key=0){
      int buffer = key % buffers.size();
      return buffers[buffer]->Try_push(data);
   }
   bool Try_pop(T* &ptr, unsigned int key=0){
      int buffer = key % buffers.size();
      return buffers[buffer]->Try_pop(ptr);
   }
   bool Try_push(std::vector<T*> &data, unsigned int key=0){
      int buffer = key % buffers.size();
      return buffers[buffer]->Try_push(data);
   }
   bool Try_pop(std::vector<T*> &ptr, unsigned int key=0){
      int buffer = key % buffers.size();
      return buffers[buffer]->Try_pop(ptr);
   }
   bool Try_push(std::vector<T*> &data, std::vector<unsigned int> &keys){
      bool ret = true;
      std::vector<bool> enable(keys.size());

      for(unsigned int ibuffer=0; ibuffer<buffers.size(); ibuffer++){
         for(unsigned int iKey=0; iKey<keys.size(); iKey++){
            enable[iKey] = ((keys[iKey] % buffers.size()) == ibuffer);
         }
         ret &= buffers[ibuffer]->Try_push(data, enable);
         //if (!ret) printf("cannot push into %d\n", ibuffer);
      }

      return ret;
   }

   unsigned int GetSize(){
      return buffers[0]->GetSize();
      unsigned int size = 0;
      for(auto b: buffers)
         size += b->GetSize();
      return size;
   }
   void Clean(){
      for(auto b: buffers){
         b->Clean();
      }
   }

   //Getters
   unsigned int GetMaxSize(){ 
      unsigned int maxSize = 0;
      for(auto b: buffers)
         maxSize += b->GetMaxSize();
      return maxSize;
   }
   float GetOccupancy(){
      float occupancy = 0.;
      for(auto b: buffers)
         occupancy += b->GetOccupancy();
      return occupancy/buffers.size();
   }//NOTE: only for monitoring
   unsigned int GetNBuffers(){
      return buffers.size();
   }
   DAQBuffer<T>* GetBufferAt(unsigned int i){ return buffers[i]; }

   //Constructor  
   DAQFanoutBuffer(unsigned int N=1, unsigned int maxsize = 0, std::string name = "NEWBUFFER", DAQSystem* parent = nullptr): DAQBufferBase(parent, name){ 
      for(size_t i=0; i<N; i++){
         buffers.push_back(new DAQBuffer<T>(maxsize, name, nullptr)); // do not expose this buffer to the main DAQSystem
      }
   }

   //Destructor
   ~DAQFanoutBuffer(){
      for(auto p: buffers){
         delete p;
      }
   }
};

// --- DAQ Thread --- basic thread wrapper
class DAQThread{
   friend class DAQSystem;
   protected:
      std::chrono::high_resolution_clock::duration fIdleLoopDuration; //allows to avoid polling too much
      std::chrono::high_resolution_clock::duration fLastLoopDuration; //for monitoring
      unsigned int fThreadId;
      std::string fThreadName;
      static std::atomic<unsigned int> fThreadCount;
      DAQSystem *fSystem;
   private:
      std::thread fThread;
      volatile bool fStarted;
      volatile bool fStop;
      volatile bool fRunning;
      volatile bool fRunning_old;

      //reserved Methods
      void ThreadMain();

      //to be implemented in derived class to setup functionalities
      virtual void Setup(){;} //called before thread Loop
      virtual void Begin(){;} //called before thread Loop
      virtual void Loop(){;}  //called inside thread Loop
      virtual void End(){;} //called before thread Loop
      virtual void Close(){;}  //called at the end of thread Loop

   public:
      //Methods
      void Start();
      void Stop();

      void GoRun();
      void StopRun();

      bool IsStarted(){
         return fStarted;
      }

      bool IsRunning(){
         return fRunning_old;
      }
      /*std::chrono::microseconds GetLastLoopDuration(){
         return std::chrono::duration_cast<std::chrono::microseconds>(fLastLoopDuration);
      }*/
      const std::string GetThreadName(){
         return fThreadName;
      }

      DAQSystem* GetSystem(){
         return fSystem;
      }

      //setter
      void SetIdleLoopDuration(std::chrono::microseconds d){fIdleLoopDuration = std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(d); }

      //Constructor
      DAQThread(DAQSystem* parent = nullptr, std::string name = "NEWTHREAD");

      //Destructor
      virtual ~DAQThread(){
         Stop();
      }
};

// --- DAQ Network Thread --- thread with socket functionalities
#define MAXUDPSIZE 9000
#define MAXMSG 200
class DAQServerThread : public DAQThread{
   private:

      //define missing stuff for replacing recvmmsg on apple systems
#if __APPLE__
      struct mmsghdr {
         struct msghdr msg_hdr;  /* Message header */
         unsigned int  msg_len;  /* Number of received bytes for header */
      };

#define MSG_WAITFORONE 0
      int recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
                   unsigned int flags, struct timespec *timeout){
         msgvec[0].msg_len = recvmsg(sockfd, &msgvec[0].msg_hdr, 0);

         return 1;
      }
#endif

      int fDataSocket;
      volatile int fServerPort;
      unsigned char fDatagramBuffer[MAXMSG][MAXUDPSIZE];
      struct mmsghdr fMsgs[MAXMSG];
      struct iovec fIoVecs[MAXMSG];
      struct sockaddr_in fAddresses[MAXMSG];
      char fSrcAddress[INET_ADDRSTRLEN];
      int fRecvMsg;
      struct timespec fTimeout;

      int fBufferSize;
      std::chrono::microseconds fDataWaitDuration;

      //reserved Methods
      void Setup();

      void Loop();


      void Close(){
         close(fDataSocket);
         fServerPort = 0;
      }

      //to be implemented in derived class to setup functionalities
      virtual void GotData() { };

   protected:
      //helper functions to be used in GotData
      unsigned int GetMessages() { return  fRecvMsg; }
      char* GetMessageSourceAddress(unsigned int id);
      unsigned char* GetMessageData(unsigned int id);
      unsigned int GetMessageSize(unsigned int id);

   public:
      //Methods
      void Clean(){
         //TODO: clean fDataSocket kernel buffer
      }

      void SetServerPort(int port){
         if(fServerPort==0) fServerPort = port;
      }

      void SetDataWaitDuration(std::chrono::microseconds d){
         fDataWaitDuration = d;
      }

      //Getter
      int GetServerPort(){ return fServerPort; }
      int GetReceivedMessages(){ return fRecvMsg; }

      //Constructor
      DAQServerThread(int buffersize=-1, DAQSystem* parent=nullptr, std::string name="SERVERTHREAD");

      //Destructor
      virtual ~DAQServerThread(){
      }
};

// --- DAQ System --- grouping of threads and buffers
class DAQSystem {
   std::vector<DAQBufferBase*> fBuffers;
   std::vector<DAQThread*> fThreads;
   DAQAlarm *fAlarms;

   public:
      //Methods
      void Start();
      void Stop();

      void WaitRunStarted();
      void WaitRunStopped();
      void WaitStopped();

      void GoRun();
      void StopRun();

      void CleanBuffers();

      DAQAlarm* GetAlarms(){
         return fAlarms;
      }

      void AddThread(DAQThread* thread);
      void AddBuffer(DAQBufferBase* buffer);

      //Constructor
      DAQSystem();

      //Destructor
      ~DAQSystem();
      
};

#endif
