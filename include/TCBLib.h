//
//  TCBLib.h
//  Trigger Concentrator Board Library Header File
//
//  Created by Luca Galli on 12/12/2015
//

#define RHW                0x00       // hardware version information required by WaveDAQ
#define RRUN               0x01       // run control
#define RBUSDLY            0x02       // trigger bus delay
#define RNTRG              0x03       // trigger enable bits
#define RALGSEL            0x04       // algorithm select on TCB1/2_0
#define RPLLRES            0x05       // Reset PLLs
#define RBOARDID           0x06       // UDP Header info register
#define PRESCADC           0x07       // ADC readout prescaling
#define RSYNCWFM           0x08       // SYNC Waveform from serdes
#define RPAYLOADMAX        0x09       // Max Packet payload
#define RLOCK              0x0A       // PLL Lock bits     
#define RLED               0x0B       // Frontpanel LED status
#define USR_ACCESS         0x0F       // FW compilaiton date
#define RENA               0x20       // trigger enable (first address)
#define RTRIPATT           0x30       // trigger pattern (first address)
#define RTRGFORCE          0x40       // force a trigger (first address)
#define RCMD               0xFF       // daq state machine control
#define RPRESCA            0x100      // prescaling value first address
#define RTOTTIME           0x200      // total time
#define RLIVETIME          0x201      // live time
#define REVECOU            0x202      // event counter
#define RTRITYPE           0x203      // trigger type
#define RSYSEVECOU         0x204      // system (trgbus) event counter
#define RSYSTRITYPE        0x205      // system (trgbus) trigger type
#define RPCURR             0x206      // proton current
#define RFSTIME            0x207      // time counter from external oscillator
#define RSYSERRCOU         0x208      // counter of errors on trigger bus
#define RSYSVALIDCOU       0x209      // counter of valid trgbus received
#define RSERDESTX          0x321      // tx serdes configuration and delay load
#define RSERDESCALIBFSM    0x326      // individual bitslip for dcb
#define RSERDESMSK         0x327      // mask for input serdes
#define RSERDESSTATUS      0x330      // readout of serdes configuration
#define RSERDESBUSY        0x351      // autolock serdes busy
#define RSERDESFAIL        0x352      // autolock serdes fail
#define RDCBREADY          0x353      // Serdes Data From DCB
#define RSERDESALIGNDLY    0x354      // autolock serdes slot delay enable
#define RSERDESALIGNDELTA0 0x355      // autolock serdes measured time offsets
#define RSERDESALIGNDELTA1 0x356      // autolock serdes measured time offsets
#define RSERDESMINLATENCY  0x357      // autolock serdes slot with minimum latency
#define RSERDESDLYSTATE    0x360      // autolock serdes sate for each delay
#define RSERDESDLYTEST     0x380      // autolock serdes tested for each delay
#define RTRGCOU            0x400      // trigger counter (first address)
#define RTRGDLY            0x500      // trigger delay (first address)
#define RPARAM             0x600      // start of parameter space
#define RQHTHR             0x600      // sum threshold high
#define RTILEMSK0          0x601      // tile mask 0
#define RTILEMSK1          0x602      // tile mask 1
#define RTILEMSK2          0x603      // tile mask 2
#define RTILEMSK3          0x604      // tile mask 3
#define RQLTHR             0x606      // sum threshold low
#define RQCTHR             0x607      // sum threshold cosmic
#define RLXePATCHID        0x608      // LXe patch request
#define RTCOFFSET          0x609      // TC time offset in TDC units
#define RTIMEN             0x60A      // Time Difference threshold Narrow
#define RTIMEW             0x60B      // Time Difference threshold Wide
#define RTCTRACKMULTTHR    0x60C      // TC multiplicity threshold
#define RTCTRACKTIMETHR    0x60D      // TC time cut
#define RRDCLYSOTHR        0x60E      // RDC QSUM threshold
#define RBGOTHR            0x60F      // BGO QSUM threshold
#define RRDCMASK           0x610      // RDC trigger definition
#define RBGOMASK           0x611      // BGO trigger definition
#define RALPHATHR          0x612      // threshold alpha
#define RALPHAPEAK         0x613      // alpha peak scale
#define RQSUMSEL           0x614      // selects the qsum as std or running average
#define RBGOVETOTHR        0x615      // BGO QSUM veto threshold
#define RTCMULTITHR        0x616      // TC Multiplicity threshold
#define RNGENDLY           0x617      // Delay of NGEN Window
#define RNGENWIDTH         0x618      // Width of NGEN Window
#define RLXeNGENQH         0x619      // LXe threshold for NGEN TRG
#define RLXeNGENQL         0x61A      // LXe veto threshold for NGEN TRG
#define RBGOHITDLY         0x61B      // Delay of BGO and Preshower hit
#define RCRCHITMASK        0x61C      // Masking bit for CRC counters (7:0)
#define RCRCPAIRENA        0x61D      // Enable bits for CRC top-bottom coincidences
#define RMAJORITYVALUE     0x61E      // Majority trigger parameters
#define RTOFXMASKS         0x61F      // Tof X-view masks
#define RTOFYMASKS         0x621      // Tof Y-view masks
#define RINTERSPILLDLY     0x623      // Interspill delay value
#define RMATRIXMASKS0      0x624      // Mask Matrix
#define RMATRIXMASKS1      0x625      // Mask Matrix
#define RMATRIXMASKS2      0x626      // Mask Matrix
#define RFNEUTRONMASKS     0x628      // Mask for FOOT neutron detector inputs
#define RLXeMPPCTDCTHR     0x629      // Minimum number of hits in a XEC MPPC patch
#define RLXePATCHDLY       0x62A      // Delay of XEC patch algorithm
#define RRDCVETOTHR        0x62B      // Veto for RDC signals
#define RRDCHITDLY         0x62C      // Delay of RDC Hits
#define RRDCHITMASK0       0x62D      // RDC Hit masks (3 values)
#define RRDCHITMASK1       0x62E      // RDC Hit masks (3 values)
#define RRDCHITMASK2       0x62F      // RDC Hit masks (3 values)
#define RDETECTORDLY0      0x630      // Detector delays: XEC bit [7:0], TC bit [15:8], CDCH [23:16], BGO [31:24] (2 values)
#define RDETECTORDLY1      0x631      // Detector delays: RDC bit [7:0], CRC bit [15:8] (2 values)#
#define RCDCHMASKS0        0x632      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS1        0x633      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS2        0x634      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS3        0x635      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS4        0x636      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS5        0x637      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS6        0x638      // CDCH WIRE MASKS ON TCB1
#define RCDCHMASKS7        0x639      // CDCH WIRE MASKS ON TCB1
#define RLXePMTTDCTHR      0x63A      // Minimum number of hits in a XEC PMT patch
#define RLXeMPPCWFMTHR     0x63B      // Minimum amplitude in a XEC MPPC waveform
#define RCDCHMULTTHR       0x640      // CDCH HIT MULTIPLICITY THRESHOLD
#define RLOLXMASK          0x641      // LoLX HIT MASKS
#define RLOLXMAJVAL        0x642      // LoLX MAJORITY VALUES
#define RTCTRACKMASK       0x643      // TC masks on reconstructed tracks, high to enable mask: bit [3:0]
#define RTDCDLY            0x644      // TDC Delay for different detectors: XEC bit [7:0], TC bit [15:8]
#define RCOMBDLY           0x645      // Delay for combined triggers: Time bit [7:0], Direction Match bit [15:8]
#define RCDCHTRGMASK       0x646      // Mask for CDCH trigger combination
#define RFCALOMASKS0       0x650      // Mask for FOOT calorimenter inputs reg 0
#define RFCALOMASKS1       0x651      // Mask for FOOT calorimenter inputs reg 1
#define RFCALOMASKS2       0x652      // Mask for FOOT calorimenter inputs reg 2
#define RFCALOMASKSTCB10   0x653      // Mask for FOOT calorimenter inputs for TCB1 reg 0
#define RFCALOMASKSTCB11   0x654      // Mask for FOOT calorimenter inputs for TCB1 reg 1
#define RFCALOMASKSTCB12   0x655      // Mask for FOOT calorimenter inputs for TCB1 reg 2
#define RFCALOMASKSTCB13   0x656      // Mask for FOOT calorimenter inputs for TCB1 reg 3
#define RFCALOMASKSTCB14   0x657      // Mask for FOOT calorimenter inputs for TCB1 reg 4
#define RFCALOMASKSTCB15   0x658      // Mask for FOOT calorimenter inputs for TCB1 reg 5
#define RFCALOMASKSTCB16   0x659      // Mask for FOOT calorimenter inputs for TCB1 reg 6
#define RFCALOMASKSTCB17   0x65A      // Mask for FOOT calorimenter inputs for TCB1 reg 7
#define RFPROGCOUVETO      0x660      // Programmable veto for Margherita Majority
#define RTHRREDUCTION      0x661      // Threshold reduction for LXe
#define RFIBCOUNTER        0x700      // SCIFI fiber event counter address (42 values)
#define RSINGLECRATECFG    0x800      // configurations for single crate trigger logic
#define RSINGLEISVETO      0x801      // veto set for input channels in single crate logic
#define RSINGLEMASK        0x809      // mask for input channels in single crate logic
#define RSINGLELOGIC       0x811      // first stage configuration in single crate logic
#define RTRGCOUSPI         0x900      // trigger counters for spi access (first address)
#define RTOTTIMESPI        0x940      // total time for spi access
#define RLIVETIMESPI       0x941      // live time for spi access
#define RLATCHCOUSPI       0x942      // latch for spi access counters
#define RALGCLKMEMADDR     0x0FFFE    // counter stop position for ALGCLK memories
#define RMEMADDR           0x0FFFF    // counter stop position
#define MEMBASEADDR        0x10000    //base address for memories
#define GENTMEMBASE        0x12000    //base address for trigger generation memories (two memories with size = GENTDIM)

/// Detector memories (size = MEMDIM)
#define XECMEMBASE           0x11100                   // XEC memories base address (2 memories)
#define BGOMEMBASE           0x13000                   // BGO memories base address (1 memory)
#define RDCMEMBASE           0x14000                   // RDC memories base address (2 memories)
#define TCMEMBASE            0x15000                   // TC memories base address (2 memories)
#define ALFAMEMBASE          0x16000                   // ALFA memories base address (2 memories)
#define CDCHMEMBASE          0x17000                   // CDCH memories base address (1 memories)
#define MPPCMEMBASE          0x18000                   // XEC MPPC memory base address (1 memory)
#define PMTMEMBASE           0x18800                   // XEC PMT memory base address (1 memory)
#define MPPCTIMEMEMBASE      0x19000                   // XEC MPPC Time memory base address (1 memory)
#define PMTTIMEMEMBASE       0x19800                   // XEC PMT Time memory base address (1 memory)
#define MPPCWAVEFORMMEMBASE  0x1A000                   // XEC MPPC MAX Waveform memory base address (1 memory)

#define SCIFICOINCBASE     0x00800000           //SCIFI fiber coincidence base address (441)

#define PACKAGERBASE       0x01000000 //base address for packager memories
#define RARBITER           0x01001000 //Bus Arbiter register and packager controller
#define BUFFERBASE         0x02000000 //Buffer base address
#define PACKAGERREGS       0x03000000 //PackagerRegs base address

///////////////////////////////////////////////////////////
// LIBRARY ASSOCIATED TO TCB_X_0
#define MEMNUM             34
#define MEMDIM             128
#define GENTDIM            32
#define BUFFERSIZE         8192
#define BUFFERNUM          4
#define PACKAGERSIZE       1024
#define BLTSIZE            32
#define NSCIFI             42
///////////////////////////////////////////////////////////

//derived addresses (should not touch!)
#define PACK_NEXT_BUFFER   BUFFERBASE+BUFFERSIZE
#define PACK_A             PACKAGERREGS
#define PACK_B             PACKAGERREGS+1
#define PACK_C             PACKAGERREGS+2
#define PACK_RADDR         PACKAGERREGS+3
#define PACK_WADDR         PACKAGERREGS+4
#define PACK_FLAGS         PACKAGERREGS+8
#define PACK_SUM           PACKAGERREGS+9
#define PACK_AND           PACKAGERREGS+10
#define PACK_OR            PACKAGERREGS+11
#define PACK_XOR           PACKAGERREGS+12

#include "sys/types.h"
#include "strlcpy.h"
#include <vector>

#include <stdexcept>
#include "DCBLib.h"

#ifndef TCBLIB_H
#define TCBLIB_H


enum PACKETIZER_COMMAND {STOP, COPY, BLOCK_COPY, DIRECT_WRITE, JUMP, JUMP_IF};

typedef struct {
   int offset;
   PACKETIZER_COMMAND cmd;
   unsigned int arg0;
   unsigned int arg1;
   unsigned int arg2;
} PacketInstruction;

class TCB {
private:
   //internal mscb connections
   int            fmscb_addr;        // MSCB address of CMB
   int            fh;                // MSCB handle
   //internal DCB
   DCB*           fDCB;              // DCB interface

public:
   // board info
   u_int32_t      fidcode;           // reg id
   u_int32_t      fexpid;            // reg experiment id
   u_int32_t      fslot;             // slot
   int            fntrg;             // number of available trigger
   int            fverbose;          // verbosity level
   int            fnserdes;          // number of available trigger
   // getters
   u_int32_t      GetIDCode() { return fidcode; }
   u_int32_t      GetSlotNum() { return fslot; }
   int            GetNSerdes() { return fnserdes; }
   
   // Constructors
   TCB(int verbose = 0) {
      fh = -1;
      fDCB = nullptr;
   };

   TCB(const char *mscb_device, int mscb_addr, int slot, int verbose = 0) {
      //open mscb connection
      char fmscb_device[128];
      strlcpy(fmscb_device, mscb_device, sizeof(fmscb_device));
      fh = mscb_init(fmscb_device, 0, "", 0);
      fmscb_addr = mscb_addr;

      fslot = slot;

      //dummy stuff
      fDCB = nullptr;
      fidcode = 0xffff;
      fntrg = 0x0;
      fverbose = verbose;
      fnserdes= 0;
   };

   TCB(const std::string &dcb_name, int slot, int verbose = 0) {
      //open mscb connection
      fh = -1;
      fDCB = new DCB(dcb_name, verbose);
      fDCB->Connect();

      fslot = slot;

      //dummy stuff
      fidcode = 0xffff;
      fntrg = 0x0;
      fverbose = verbose;
      fnserdes= 0;
   };


   //set mscb/Dcb access
   void SetMscbHandle(int handle, int slot, int addr=20){
      fh = handle;
      fmscb_addr = addr;
      fslot = slot;
   };

   void SetDcbInterface(DCB* dcb, int slot){
      fDCB = dcb;
      fslot = slot;
   };

   bool HasDcbInterface(){
      return fDCB != nullptr;
   }

   bool HasMscbInterface(){
      return fh != -1;
   }

   //general write register function
   void WriteReg(u_int32_t, u_int32_t*);
   //general write block transfert function
   void WriteBLT(u_int32_t, u_int32_t*,int);
   // general read register function
   void ReadReg(u_int32_t, u_int32_t*);
   // general read block transfer function
   void ReadBLT(u_int32_t, u_int32_t*,int);
   // prescaling values setting
   void SetPrescaling(u_int32_t*);
   // read prescaling values
   void GetPrescaling(u_int32_t*);
   // read LXePatch LUT
   void GetLXePatchLUT(u_int32_t*);
   // write LXePatch LUT
   void SetLXePatchLUT(u_int32_t*);
   // set the IDCode
   void SetIDCode();
   // set the BoardID, CrateID and Slot
   void SetBoardId(u_int32_t boardid, u_int32_t crateid, u_int32_t slotid);
   //get BoardID, CrateID and Slot
   void GetBoardId(u_int32_t *boardid, u_int32_t *crateid, u_int32_t *slotid);
   // set fntrg
   void SetNTRG();
   // write a memory
   void WriteMemory(int,u_int32_t*);
   // write a memory
   void WriteMemoryBLT(int,u_int32_t*);
   // read a memory
   void ReadMemory(int,u_int32_t*);
   // read all memories
   void ReadMemoryBLT(int,u_int32_t*);
   //fill an address range with a given value
   void FillMemory(unsigned int address, unsigned int len, unsigned int val);
   // set the runmode
   void GoRun();
   // get the runmode status
   int IsRunning();
   // remove the busy
   void RemoveBusy();
   // software sync
   void SWSync();
   // software stop
   void SWStop();
   // set rrun register
   void SetRRUN(u_int32_t*);
   // set rena register
   void SetRENA(u_int32_t*,int);
   // set rena register
   void SetTriggerEnable(bool *);
   // set ralgsel register
   void SetRALGSEL(u_int32_t*);
   // get rrun register
   void GetRRUN(u_int32_t*);
   // get rena register
   void GetRENA(u_int32_t*, int);
   // get ralgsel register
   void GetRALGSEL(u_int32_t*);
   // read PLL Lock
   unsigned int GetLock();
   // read LED
   unsigned int GetLed();
   // read total time
   void GetTotalTime(u_int32_t*);
   // read total time through SPI
   void GetTotalTimeSPI(u_int32_t*);
   // read live time
   void GetLiveTime(u_int32_t*);
   // read live time through SPI
   void GetLiveTimeSPI(u_int32_t*);
   // read event counter
   void GetEventCounter(u_int32_t*);
   // read trigger type
   void GetTriggerType(u_int32_t*);
   // read trigger pattern
   void GetTriggerPattern(u_int32_t*, int);
   // read system event counter
   void GetSystemEventCounter(u_int32_t*);
   // read system trigger type
   bool GetSystemTriggerType(u_int32_t*, u_int32_t*, u_int32_t*);
   // read trgbus error counter
   void GetTrgbusErrorCounter(u_int32_t*);
   // read trgbus Valid counter
   void GetTrgbusValidCounter(u_int32_t*);
   // read trigger counters
   void GetTriggerCounters(u_int32_t*);
   // read trigger counters through SPI
   void GetTriggerCountersSPI(u_int32_t*);
   // read trigger counter and timers through SPI
   void ReadCounters(u_int32_t *);
   // read memory address
   void GetMemoryAddress(u_int32_t*);
   // check if the system is busy
   int IsBusy();
   //check DCB is ready
   bool IsDcbReady();
   // write in trg bus Odelay register
   void SetTRGBusODLY(u_int32_t*,u_int32_t*,u_int32_t*);
   // write in trg bus Idelay register
   void SetTRGBusIDLY(u_int32_t*,u_int32_t*,u_int32_t*);
   // write in trg bus I/O delay register
   void GetTRGBusDLY(u_int32_t*,u_int32_t*,u_int32_t*,u_int32_t*,u_int32_t*,u_int32_t*);
   // write serdes memory
   void WriteSERDESMem(int,int,u_int32_t*);
   // read serdes memory
   void ReadSERDESMem(int,int,u_int32_t*);
   // Serdes delay values setting
   void SetSerdesMask(u_int32_t*);
   // Serdes delay values setting
   void GetSerdesMask(u_int32_t*);
   // set generic trigger parameter (with base address 0x600)
   void SetParameter(u_int32_t, u_int32_t*);
   // get FW compilation date
   void GetCompilDate(u_int32_t*);
   // force the trigger passed by the function call
   void ForceTrigger(int);
   //reset transmitter
   void ResetIDLYCTRL();
   //reset transmitter
   void ResetTransmitter();
   //set variable pattern
   void SetSerdesPattern(bool);
   //send input serdes to algorims and memory
   void SetFadcMode(bool);
   //send output memory to serdes
   void SetTestTxMode(bool);
   //send TRG, SYNC and BUSY mask
   void SetTRGBusMasks(bool, bool, bool);
   //Assign Bus to Packetizer
   void SetPacketizerBus(bool);
   //Check local bus association
   bool GetPacketizerBus();
   //Set Packetizer enable
   void SetPacketizerEnable(bool);
   //Set Packetizer autostart
   void SetPacketizerAutostart(bool);
   //issue a software start to packetizer
   void StartPacketizer();
   //force stop packetizer
   void AbortPacketizer();
   //write Command to packetizer memories
   void SetPacketizerCommandAt(int offset, PACKETIZER_COMMAND cmd, u_int32_t arg0, u_int32_t arg1, u_int32_t opt=0);
   //Set ReadoutFSM enable
   void SetReadoutEnable(bool);
   //Set Max Payload Size
   void SetMaxPayload(u_int32_t*);
   //Set Max Payload Size
   void GetMaxPayload(u_int32_t*);
   //read current Buffer content
   void ReadBuffer(u_int32_t* ptr, int size = (BUFFERSIZE-1), int offset = 0);
   //increment Buffer pointer
   void IncrementBufferPointer();
   //reset Buffer busy logic
   void ResetBufferLogic();
   //get current SPI Buffer pointer;
   int GetSPIBufferPointer();
   //get current Packetizer bus pointer;
   int GetPacketizerBufferPointer();
   //get current buffer memory state
   u_int32_t GetBufferState();
   //reset PLLs
   void ResetPLLs();
   //read the PLL unlock counter PLLs
   void GetPLLUnlockCou(u_int32_t *data);
   //reset the PLL unlock counter PLLs
   void ResetPLLUnlockCou();
   //Set adc prescaling readout value
   void SetPrescAdc(u_int32_t *);
   //Get adc prescaling readout value
   void GetPrescAdc(u_int32_t *);
   //Set trigger delay
   void SetTRGDLY(bool *, u_int32_t *);
   //Get trigger delay
   void GetTRGDLY(u_int32_t *);
   //Start AutoLock
   void AutoCalibrateSerdes();
   //Read Current Serdes
   void ReadCurrentSerdes(u_int32_t *dlyout, int *bitout);
   //Get AutoLock Fail
   void GetAutoCalibrateFail(u_int32_t* ret);
   //Get AutoLock Fail
   void GetAutoCalibrateBusy(u_int32_t* ret);
   //perform dummy calibration and retrieve serdes eyes
   void GetAutoCalibrateEye(u_int32_t* eyes);
   //Returns enable value for latency compensations of SerDes
   void GetAutoAlignDlys(u_int32_t* ret);
   //single crate configuration
   void SetSingleCrateConfiguration(bool useGlobalAnd, short shape, short vetoShape);
   void SetSingleCrateChnMask(bool *state);
   void SetSingleCrateChnIsVeto(bool *state);
   void SetSingleCrateChnLogic(bool *state);
   void SetSingleCrateTriggerOr(int nChn, int* chn, short shape);
   void SetSingleCrateTriggerAnd(int nChn, int* chn, short shape);
   void SetFMask(bool, bool);
   void SetLocalTrigger(bool ena);
   //write full Packetizer program
   void WritePacketizerProgram(std::vector<PacketInstruction> &list);
   //read buffer head, return buffer read pointer
   u_int32_t GetBufferHeadSPI(int *nBanks, u_int32_t *evecou=0, u_int32_t *totaltime=0, u_int32_t* sys_tritype=0, u_int32_t *sys_evecou=0);
   //check has bank
   bool HasBufferBankSPI(u_int32_t ptr, char *bankName, int *length);
   //skip bank
   u_int32_t SkipBufferBankSPI(u_int32_t ptr, int length);
   //get bank data
   void GetBufferBankDataSPI(u_int32_t ptr, u_int32_t *data, int length);
   //get SYNC waveform
   void GetSyncWaveform(u_int32_t *ptr);
   //reset SYNC waveform serdes
   void ResetSyncWaveformSerdes();

   //trigger-specific calls
   void SetDetectorDelay(bool *enable, u_int32_t* value);
   void SetTdcDelay(bool* enable, u_int32_t*value);
   void SetCombinedConditionDelay(bool* enable, u_int32_t*value);
   void SetLXeThrReduction(bool enable, float value);
   // Set waveform sum trigger threshold
   void SetSumHighThreshold(u_int32_t*);
   void SetSumLowThreshold(u_int32_t*);
   void SetSumVetoThreshold(u_int32_t*);
   //Xenon patch position selection
   void SetPatch(u_int32_t*);
   void SetMppcNTdcThreshold(u_int32_t*);
   void SetPmtNTdcThreshold(u_int32_t*);
   void SetMppcWaveformThreshold(u_int32_t*);
   void SetPatchDelay(bool, u_int32_t);
   //time thresholds
   void SetTimeNarrow(u_int32_t*);
   void SetTimeWide(u_int32_t*);
   //Alpha configuration
   void SetAlphaThreshold(u_int32_t*);
   void SetAlphaPeakScale(float);
   //qsum std or moving average
   void SetQsumMovingAverage(bool);
   void SetQsumPMTMultiplier(int);
   //MAX selection (NTDC-QSUM)
   void SetXECMaxFromQsum(bool);
   //Time selection (MPPC-PMT)
   void SetXECTimeFromPMT(bool);
   // Set TC masks
   void SetTCMasks(u_int32_t*);
   void SetTCMultiplicityThreshold(u_int32_t*);
   void SetTCTrackMultiplicityThreshold(u_int32_t*);
   void SetTCTrackTimeThreshold(u_int32_t*);
   void SetTCTimeOffset(u_int32_t*);
   void SetTCTrackMask(u_int32_t*);
   //Set BGO Stuff
   void SetBGOThreshold(u_int32_t *);
   void SetBGOVetoThreshold(u_int32_t *);
   void SetBGOTriggerMask(u_int32_t *);
   void SetBGOHitDelay(u_int32_t, u_int32_t);
   //Set RDC Stuff
   void SetRDCThreshold(u_int32_t *);
   void SetRDCVetoThreshold(u_int32_t *);
   void SetRDCHitDelay(u_int32_t *);
   void SetRDCHitMask(u_int32_t *);
   void SetRDCTriggerMask(u_int32_t *);
   //Set CRC Stuff
   void SetCRCHitMask(u_int32_t *);
   void SetCRCPairEnable(u_int32_t *);
   //Set NGEN Stuff
   void SetNGENDly(u_int32_t *);
   void SetNGENWidth(u_int32_t *);
   void SetNGENHighThreshold(u_int32_t *);
   void SetNGENLowThreshold(u_int32_t *);
  // Set CDCH Stuff
  void SetCDCHMasks(u_int32_t *);
  void SetCDCHTriggerMask(u_int32_t *);
  void SetCDCHUSMultThr(u_int32_t *);
  void SetCDCHDSMultThr(u_int32_t *);
  // Set LoLX Stuff
  void SetLoLXMasks(u_int32_t *);
  void SetLoLXMajScinVal(u_int32_t *);
  void SetLoLXMajCherVal(u_int32_t *); 
  void SetLoLXMajBareVal(u_int32_t *);
  //Get proton current
  void GetPCurr(u_int32_t *);
  //Get SciFi counters
  void GetSciFICou(u_int32_t *);
  // FOOT stuffs
  void SetFHitShaper(u_int32_t *);
  void SetFVetoShaper(u_int32_t *);  
  void SetMargaritaMajVal(u_int32_t *);
  void SetMargaritaTrgDly(u_int32_t *);
  void SetMargaritaMask(u_int32_t *);
  void SetTofBarHitLogic(u_int32_t *);
  void SetTofHitLogic(u_int32_t *);
  void SetTofHitLogicAlternative(u_int32_t *);
  void SetTofXMask(u_int32_t *);
  void SetTofYMask(u_int32_t *);
  void SetFCaloMask(u_int32_t *);
  void SetFCaloMaskTCB1(u_int32_t *);
  void SetFProgCouVeto(u_int32_t *);
  void SetFNeutronMask(u_int32_t *);
  void SetMatrixMask(u_int32_t *);
  void SetInterspillDly(u_int32_t *);
};

#endif
