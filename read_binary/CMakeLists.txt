cmake_minimum_required(VERSION 3.0)

project(read_binary)

add_executable(read_binary read_binary.cpp)
install(TARGETS read_binary DESTINATION bin)
