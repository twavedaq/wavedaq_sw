classdef MSO64B < handle
	
	properties (SetAccess = protected)
		
		scope;
		fftWin;
		sa;
		
		chan;
		dx;
		
		x;
		y;
		
		myFig;
	end
	
	properties (Constant)
		
		MAX_N_POINTS				= 2^24;
		EXPR						= '[-]*\d.\d+e[+-]\d+';
		
		VAL_ADAPT_BIT				= 2;
	end
	
	events
		
		recvWfData;
	end
	
	methods
		%%
		function obj = MSO64B(fftWin)
			
			if nargin < 1
				fftWin				= @rectwin;
			end
			
			validateattributes(fftWin, {'function_handle'}, {'scalar'});
			
			obj.fftWin				= fftWin;
									
			obj.init;
		end
		
		%%
		function init(obj)
			
			obj.sa					= arrayfun(@(~) freqAnalyzer(obj.fftWin), 1:4);			
			obj.scope				= visa('ni', 'USB0::0x0699::0x0530::C032569::0::INSTR');
			
			set(obj.scope, 'InputBufferSize',   obj.MAX_N_POINTS*2);
			% Does this have any effect?
			set(obj.scope, 'EOSCharCode',		'LF');
			set(obj.scope, 'EOSMode',           'read');
			
			% Callback function for asynchronous reads
			set(obj.scope, 'BytesAvailableFcn',	@obj.devCallback);

			fopen(obj.scope);

			% Disable response header
			fprintf(obj.scope, 'HEAD OFF');
			% Binary waveform data
			fprintf(obj.scope, 'WFMO:ENC BIN');
			% Two bytes per sample
			fprintf(obj.scope, 'WFMO:BIT_N 16');
			% Byte order
			fprintf(obj.scope, 'WFMO:BYT_O LSB');
			% Waveform format
			fprintf(obj.scope, 'WFMO:BN_F');
							
			% Time zone
			fprintf(obj.scope, 'TIME:ZONE "Europe/Zurich"');
			% TODO: Set date and time
			%fprintf(obj.scope, 'DATE %s', datestr(now, 'dd,mmm,yyyy,HH,MM,SS'));
			
			% Stop acquisition
			fprintf(obj.scope, 'ACQ:SEQ:MOD NUMACQ');
			fprintf(obj.scope, 'ACQ:SEQ:NUMSEQ 1');
			fprintf(obj.scope, 'ACQ:STOPA SEQ');
			
			obj.acq;
			obj.getDevSetup;
		end
		
		%%
        function acq(obj, n)
			
   			if nargin < 2
				n = 1;
			end

			% Changes from stopped to single acquisition
            fprintf(obj.scope, 'ACQ:SEQ:NUMSEQ %d', n);
			fprintf(obj.scope, 'ACQ:STATE 1');
			% Force trigger
			%fprintf(obj.scope, 'FRTR');

			% Wait for acqusition to complete
% 			fprintf(obj.scope, '*WAI;*OPC?');
% 			fscanf(obj.scope);

            % For long sequences (e.g. with 10k acquisitions per sequence)
            % the '*WAI;*OPC?' request results in a timeout.
            % Polling the busy seems to be more reasonable in this case.
            busy = '1';
            while busy == '1'
                pause(0.5);
                fprintf(obj.scope, 'BUSY?');
                busy = fscanf(obj.scope);
                busy = strip(busy);
            end
            disp('Done')
		end
		
		%%
		function acqAsync(obj)
			
			fprintf(obj.scope, 'ACQ:STATE 1;*WAI;*OPC?');
			readasync(obj.scope);
		end
			
		%%
		function getDevSetup(obj)
			
			% Activated channels
			obj.chan				= [zeros(4, 1) nan(4, 2)];
			
			fprintf(obj.scope, 'DAT:SOUR:AVAIL?');
			resp					= fscanf(obj.scope);
			k						= cellfun(@(x) str2double(x), regexp(resp, '\d', 'match'));
			
			for n = 1:length(k)
				fprintf(obj.scope, 'DAT:SOU CH%d', k(n));
				fprintf(obj.scope, 'WFMO:YMU?');
				
				% Vertical scale in V/div
				dy					= str2double(fscanf(obj.scope));
				
				fprintf(obj.scope, 'WFMO:YZE?');
												
				% Offset in V
				offs				= str2double(fscanf(obj.scope));
				
				obj.chan(k(n), :)	= [1 dy offs];
			end
				
			% Horizontal scale
			fprintf(obj.scope, 'WFMO:XIN?');
					
			obj.dx					= str2double(fscanf(obj.scope));
			obj.x					= [];
			obj.y					= [];
		end
		
		%%
		function getWf(obj)
			
			for n = 1:4
				if obj.chan(n, 1)
					fprintf(obj.scope, 'DAT:SOU CH%d', n);
					fprintf(obj.scope, 'CURV?');
					
					temp			= binblockread(obj.scope, 'int16');
					obj.y(n, :)		= obj.chan(n, 2)*temp' + obj.chan(n, 3);
					
					if isempty(obj.x)
						N           = length(temp);
						obj.x		= 0:obj.dx:(N-1)*obj.dx;
					end
					
					obj.sa(n).setTimeSignal(obj.x, obj.y(n, :)-mean(obj.y(n, :)));				
				end
			end
		end
		
		%%
		function setTimebase(obj, refFreq)
			
			tbase					= [1 2 5 10];
			
			period					= 1/refFreq;
			% 10 periods into one division
			tdiv					= 10*period;
			mag						= 10^(floor(log10(tdiv)));
			val						= tdiv/mag;
			k						= find(abs(tbase-val) == min(abs(tbase-val)));
			
			fprintf(obj.scope, 'HOR:SCA %0.3e', tbase(k)*mag); %#ok<FNDSB>
		end
		
		%%
        function plot(obj, fn, fp)
		
			saveToDisk				= true;
			if nargin < 3
				fp					= '.';
			end
			if nargin < 2
				fn					= 'hardcopy';
				saveToDisk			= false;				
			end

            obj.myFig(1)			= frontWindow('Time domain signal');
			cla;
			hold off
			
			k						= 1;
			leg						= cell(1, 4);
			for n = 1:4
				if obj.chan(n, 1)
					plot(obj.x, obj.y(n, :));
					hold on;
					
					leg{1, k}		= sprintf('Channel %d', n);
					k				= k+1;
				end
			end
			leg						= leg(1, 1:(k-1));
			
			hold off;
            xlim([0 100e-9]);
            %title('Time Domain Signal', 'FontSize', 14);
            xlabel('Time [s]');
            ylabel('Signal [V]');
			legend(leg);
			grid on;

            if saveToDisk
                set(obj.myFig(1), 'Position', 0.5.*get(0, 'Screensize'));
				fn					= sprintf('%s%s%s_Timedomain.bmp', fp, filesep, fn);
                saveas(obj.myFig(1), fn, 'bmp');
            end
			
% 			obj.myFig(2)			= frontWindow('Frequency domain signal');
% 			cla;
% 			hold off
% 			
% 			for n = 1:4
% 				if obj.chan(n, 1)
% 					semilogx(obj.sa(n).f_ss, 10*log10(obj.sa(n).S_X_ss));
% 					hold on;
% 				end
% 			end
% 			hold off;
% 			legend(leg);
% 			grid on;
		end
		
		%%
		function wdir = setWorkDir(obj)
			% Set current working directory. This is the front USB 3.0
			% port.
			fprintf(obj.scope, 'FILES:CWD "E:"');
            % select/create folder "Measurments"
            fprintf(obj.scope, 'FILES:DIR?');
            dir_content = fscanf(obj.scope);
            if ~contains(dir_content, "Measurements");
                fprintf(obj.scope, 'FILES:MKD "Measurements"');
            end
            fprintf(obj.scope, 'FILES:CWD "Measurements"');
            % select/create subfolder "<year>_<date>_<time>"
            current_date = datetime("today");
            user_name = getenv('USERNAME');
            day_dir = sprintf('%s_%d_%02d_%02d', user_name, year(current_date), month(current_date), day(current_date));
            fprintf(obj.scope, 'FILES:DIR?');
            dir_content = fscanf(obj.scope);
            if ~contains(dir_content, day_dir)
                fprintf(obj.scope, 'FILES:MKD "%s"', day_dir);
            end
            fprintf(obj.scope, 'FILES:CWD "%s"', day_dir);
            fprintf(obj.scope, 'FILES:CWD?');
            wdir = fscanf(obj.scope);
            wdir = strip(erase(wdir,'"'));
        end

    	%%
		function hardcopy(obj, fn, fp)
			
			saveToDisk				= true;
			if nargin < 3
				fp					= '.';
			end
			if nargin < 2
				fn					= 'hardcopy';
				saveToDisk			= false;				
			end
			
			validateattributes(fn, {'char'}, {'vector'});
			validateattributes(fp, {'char'}, {'vector'});

            % Set working directory on scope
            wdir = obj.setWorkDir();
			% Save image
			fprintf(obj.scope, 'SAV:IMAG "%s.bmp"', fn);
			% Read image
            pause(0.1); % Pause to make sure file is saved
            scope_file = sprintf('%s/%s.bmp', wdir, fn);
			fprintf(obj.scope, 'FILES:READF "%s"', scope_file); 
			
			data					= fread(obj.scope);
			img						= bmpImage(data(1:end-1));
			
			obj.myFig(3)			= frontWindow('Device screen');
			myImg					= image(img);
			myAxes					= get(myImg, 'Parent');
			pos						= get(obj.myFig(3), 'Position');
			width					= size(img, 2);
			height					= size(img, 1);
			
			set(myAxes, 'XTick',	[]);
			set(myAxes, 'YTick',	[]);
			set(myAxes, 'Units',	'pixels');
			set(myAxes, 'Position', [0 0 width height]);
			
			set(obj.myFig(3), 'Position', [pos(1) pos(2)+pos(4)-height width height]);
			
			if saveToDisk
				fn					= sprintf('%s%s%s.bmp', fp, filesep, fn);
				fid					= fopen(fn, 'w');
				
				if fid > 0
					fwrite(fid, uint8(data), 'uint8');
					fclose(fid);
				else
					warning('MSO64B:hardcopy', 'Failed to open file: %s', fn);
				end
			end
		end

        %%
        function getPlot(obj, nr, fn, fp, doPlot)
			
			saveToDisk				= true;
			if nargin < 5
                doPlot              = false;
            end
			if nargin < 4
				fp					= '.';
			end
			if nargin < 3
				fn					= 'hardcopy';
				saveToDisk			= false;
            end
            if nargin < 2
                nr = 1;
            end
			
			validateattributes(fn, {'char'}, {'vector'});
			validateattributes(fp, {'char'}, {'vector'});
			
            % Set working directory on scope
            wdir = obj.setWorkDir();
            % Select plot
            fprintf(obj.scope, 'DIS:SEL:VIEW PLOTVIEW%d', nr);
			% Save plot data
			fprintf(obj.scope, 'SAV:PLOTD "%s.csv"', fn);
			% Read plot data file
            pause(0.1); % Pause to make sure file is saved
            scope_file = sprintf('%s/%s.csv', wdir, fn);
			fprintf(obj.scope, 'FILES:READF "%s"', scope_file); 
 			data					= fscanf(obj.scope);

            % Read statistics
            %fprintf(obj.scope, 'MEASU:LIST?');
            %measurements = fscanf(obj.scope);
            % Read population
            fprintf(obj.scope, 'MEASU:MEAS1:RESU:ALLA:POPU?');
            stat_popu = fscanf(obj.scope);
            stat_popu = str2double(stat_popu);
            % Read mean
            fprintf(obj.scope, 'MEASU:MEAS1:RESU:ALLA:MEAN?');
            stat_mean = fscanf(obj.scope);
            stat_mean = str2double(stat_mean);
            % Read standard deviation
            fprintf(obj.scope, 'MEASU:MEAS1:RESU:ALLA:STDDEV?');
            stat_stddev = fscanf(obj.scope);
            stat_stddev = str2double(stat_stddev);
            % Read minimum
            fprintf(obj.scope, 'MEASU:MEAS1:RESU:ALLA:MIN?');
            stat_min = fscanf(obj.scope);
            stat_min = str2double(stat_min);
            % Read maximum
            fprintf(obj.scope, 'MEASU:MEAS1:RESU:ALLA:MAX?');
            stat_max = fscanf(obj.scope);
            stat_max = str2double(stat_max);

            if doPlot
     			obj.myFig(4)			= frontWindow('TIE Histogram');
                pdata = splitlines(data);
                pdata = split(pdata(1:end-2,1),',');
                %hdr = pdata(1,:);
                dbldata = str2double(pdata(2:end,:));
                bar(dbldata(:,1), dbldata(:,2));
                xline(stat_mean, 'Color', 'g', 'LineWidth', 1);
                xline(stat_mean-stat_stddev, 'Color', 'r', 'LineWidth', 1, 'LineStyle','--');
                xline(stat_mean+stat_stddev, 'Color', 'r', 'LineWidth', 1, 'LineStyle','--');
                grid on;
                stats = sprintf('   Mean = %.3e\n   Stddev = %.3e\n   Min = %.3e\n   Max = %.3e\n   Population = %d', stat_mean, stat_stddev, stat_min, stat_max, stat_popu);
                xl = xlim;
                yl = ylim;
                text(0.6*xl(2), 0.8*yl(2), stats, 'FontSize', 12);
                %title('TIE Histogram', 'FontSize', 14);
                xlabel('TIE [s]');
                ylabel('Hits');

                if saveToDisk
                    set(obj.myFig(4), 'Position', 0.5.*get(0, 'Screensize'));
    				pltFn					= sprintf('%s%s%s_TIEhistogram.bmp', fp, filesep, fn);
                    saveas(obj.myFig(4), pltFn, 'bmp');
                end
            end
			
			if saveToDisk
				fn					= sprintf('%s%s%s_TIEhistogram.csv', fp, filesep, fn);
				fid					= fopen(fn, 'w');
				
				if fid > 0
					fwrite(fid, uint8(data), 'uint8');
					fclose(fid);
				else
					warning('MSO64B:plot data transfer', 'Failed to open file: %s', fn);
				end
			end
		end
		
		%%
		function devCallback(obj, ~, event)
		
			if strcmp(event.Type, 'BytesAvailable')
				fscanf(obj.scope);
				obj.getWf;
				
				notify(obj, 'recvWfData');
			end
		end
		
		%%
		function delete(obj)
			
			if ~isempty(obj.scope)
				if isvalid(obj.scope)
					if strcmp(get(obj.scope, 'Status'), 'open')
						% Revert to auto trigger
						fprintf(obj.scope, 'ACQ:STOPA RUNST');
						fprintf(obj.scope, 'ACQ:STATE 1');
						fclose(obj.scope);
					end
					delete(obj.scope);
				end
			end
			
			for n = 1:length(obj.myFig)
				h					= figure(obj.myFig(n));
				
				if isvalid(h)
					close(h);
				end
			end
		end
		
	end % methods
end % classdef
