function wdaqJit(q, fn, fp)
    % initialize with q = MSO64B
    q.acq(10000);
    q.getWf;
    q.plot(fn, fp);
    q.getPlot(1, fn, fp, true);
    q.hardcopy(fn, fp);
end