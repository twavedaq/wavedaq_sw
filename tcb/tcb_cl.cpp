#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mscb.h"
#include "TCBLib.h"
#include <ctime>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <vector>

void str2reg(char* str, u_int32_t * reg){
   if(str[0] == '0' && str[1] == 'x'){
       *reg = strtol(str+2, NULL, 16);
   } else if(strcmp(str, "rA")==0){
       *reg = PACK_A; 
   } else if(strcmp(str, "rB")==0){
       *reg = PACK_B; 
   } else if(strcmp(str, "rC")==0){
       *reg = PACK_C; 
   } else if(strcmp(str, "rR")==0){
       *reg = PACK_RADDR; 
   } else if(strcmp(str, "rW")==0){
       *reg = PACK_WADDR; 
   } else if(strcmp(str, "rSUM")==0){
       *reg = PACK_SUM; 
   } else if(strcmp(str, "rAND")==0){
       *reg = PACK_AND; 
   } else if(strcmp(str, "rOR")==0){
       *reg = PACK_OR; 
   } else if(strcmp(str, "rXOR")==0){
       *reg = PACK_XOR; 
   } else if(strcmp(str, "aBUFF")==0){
       *reg = BUFFERBASE;
   } else if(strcmp(str, "rNEXTBUFF")==0){
       *reg = PACK_NEXT_BUFFER; 
   } else if(strcmp(str, "*")==0){
       *reg = 0x80000000; 
   } else *reg = atoi(str);
}

void MemoryRewind(int ich, u_int32_t last, u_int32_t *mem, u_int32_t *outmem) {
   *outmem = 0;
   for(int i=0;i<32;i++)
      *outmem |= ((mem[(ich/32)*32+i]>>(ich%32))&0x1)<<((32-i+last)%32);  
}

int main(int argc, char *argv[])
{
   int option;
   int handle = -1;
   DCB* DCBBoard = nullptr;
   char opline[256];
   u_int32_t data, scanfdata;
   u_int32_t trgtype, tpattern;
   FILE *filin, *filout, *filpresca, *filtrgdly;
   u_int32_t presca[128], counters[128], trgdly[128];
   bool trgdly_enable[128];
   //  clock_t t_before, t_after;
   if(argc < 2) {
      printf("Please indicate the mscb connection ID and (optionally) the slot...\n");
      return 0;
   }

   printf("interfacing with TCB_X_0 at %s:%s slot 17....\n", argv[1], argv[2]);

   // create TCB Board
   TCB TCBBoard;

   if(strncmp(argv[1], "dcb", 3)==0 || strncmp(argv[1], "DCB", 3)==0){
      printf("using DCB interface!\n");
      std::string s(argv[1]);
      DCBBoard = new DCB(s);
      DCBBoard->Connect();
      TCBBoard.SetDcbInterface(DCBBoard, atoi(argv[2]));
   } else {
      // open mscb connection
      handle = mscb_init(argv[1], 0, "", 0);
      TCBBoard.SetMscbHandle(handle, atoi(argv[2]));
   }


   TCBBoard.SetIDCode();
   TCBBoard.SetNTRG();
   TCBBoard.fverbose=1;

   /* main loop on the options */
   do {
      printf("\n  --- Options: \n");
      printf("[ 1]: Set RRUN and RENA    \t \t  [ 2]: Get RRUN and RENA reg\n");
      printf("[ 3]: Activate runmode     \t \t  [ 4]: Remove the busy\n");
      printf("[ 5]: Give a SW stop       \t \t  [ 6]: Give a SW sync \n");
      printf("[ 7]: Set prescaling       \t \t  [ 8]: \n");
      printf("[ 9]: Read TotalTime       \t \t  [10]: Read Live Time \n");
      printf("[11]: Read Event Counter   \t \t  [12]: Read trigger type\n");
      printf("[13]: Read Trigger Counters\t \t  [14]: Read memory address\n");
      printf("[15]: Select Board         \t \t  [16]: Set trg bus delay\n");
      printf("[17]: Write SERDES mem     \t \t  [18]: Read SERDES mem\n");
      printf("[19]:                      \t \t  [20]: \n");
      printf("[21]:                      \t \t  [22]: \n");
      printf("[23]: Write SERDES Mask    \t \t  [24]: Set Parameter\n");
      printf("[25]:                      \t \t  [26]: \n");
      printf("[27]: Force a trigger      \t \t  [28]: Reset Transmitter\n");
      printf("[29]:                      \t \t  [30]: Dump Data \n");
      printf("[31]: Packetizer Commands  \t \t  [32]: Buffer Commands\n");
      printf("[33]: Reset PLL            \t \t  [34]: Reset PLL unlock cou\n");
      printf("[35]: Read Unlock counter  \t \t  [36]: Get prescaling\n");
      printf("[37]: Set trigger delay    \t \t  [38]: Get trigger delay\n");
      printf("[39]: LockSerdes FSM Start \t \t  [40]: Get current serdes values\n");
      printf("[41]: LockSerdes Status    \t \t  [42]: LockSerdes Draw Eye      \n");
      printf("[43]: Single crate TRG conf\t \t  [44]: Read Prescaling          \n");      
      printf("[45]: SCIFI run            \t \t  [46]: Read Register \n");
      printf("[47]: Write Register       \t \t  [-1]: Exit          \n");
      //printf("[-1]: Exit\n");

      do {
         printf("Give an option: ");
         scanf("%s",opline);
         option = strtod(opline,NULL);
      } while ( option == 0 ) ;
      //
      if(option == 1) {
        printf(" opt = 1 : Set RRUN ... \n");
        printf("\nFADCMODE?\n if 0 input RAMs are not written by SERDES data\n if 1 the opposite\n"); 
        scanf("%x",&scanfdata);
        data = scanfdata<<2;
        printf("\nTESTTXMODE?\n if 1 output RAMs are not written by processed data\n if 0 the opposite\n"); 
        scanf("%x",&scanfdata);
        data |= scanfdata<<5;
        if(((TCBBoard.fidcode&0xf000)>>12)==2 || ((TCBBoard.fidcode&0xf000)>>12)==1) {
          printf("\nPATTERN_SERDES?\n if 1 serdes calibration pattern is set on any SERDES transmission\n");
          scanf("%x",&scanfdata);
          data |= scanfdata<<9;
        }
        printf("\nENABLE_TRGBUS?\n If 0 then the TRGBus from backplane is not used but internal signals \n if 1 then the backplane signals are used\n");
        scanf("%x",&scanfdata);
        data |= scanfdata<<4;
        if(((TCBBoard.fidcode&0xf000)>>12)==3) {
          printf("\nENABLE_LOCALTRG?\n if 1 a copy of the FCI triggerbus is sent to the DCB through backplane\n");
          scanf("%x",&scanfdata);
          data |= scanfdata<<11;
        }
        printf("\nMASKS? (hex) \n bit 0 is the EXBUSY \n bit 1 for the SYNC\n bit 2 for the TRG\n");
        scanf("%x",&scanfdata);
        data |= scanfdata<<13; 
        TCBBoard.SetRRUN(&data);
        if(((TCBBoard.fidcode&0xf000)>>12)==3) {
          int nword = (TCBBoard.fntrg-1)/32 + 1;
          for(int iword = 0; iword <nword; iword++){
            printf("\nTRGENA?(hex, bit [%d:%d])\n",(iword+1)*32-1,iword*32);
            scanf("%x",&scanfdata);
            data = scanfdata;
            TCBBoard.SetRENA(&data,iword);
          }
        }
        if(((TCBBoard.fidcode&0xf000)>>12)!=3) {
          printf("\nALGSEL?(hex)\n each bit is associated with a trigger ID as in the trigger map \n");
          scanf("%x",&scanfdata);
          data = scanfdata;
          TCBBoard.SetRALGSEL(&data);
        }
      }
      //
      if(option == 2) {
         printf(" opt = 2 : Get RRUN ... \n");

         printf(" FW compilation date: ");
         TCBBoard.GetCompilDate(&data);
         printf("%02d/%02d/20%02d %02d:%02d:%02d\n",(data&0xF8000000)>>27,(data&0x7800000)>>23,(data&0x7e0000)>>17,(data&0x1F000)>>12,(data&0xFC0)>>6,(data&0x3F));

         TCBBoard.GetRRUN(&data);

         if((data&0xF000000)>>24==0xF) {
            printf("\n*********************************************************************************************************************\n");
            printf("** Board configured to drive the trigger bus to the front panel connector, check the cable to the Ancillary Master! **\n");
            printf("*********************************************************************************************************************\n\n");
         }   
         else if((data&0xF000000)>>24==0xB) {
            printf("\n****************************************************************\n");
            printf("** Board configured to drive the trigger bus to the backplane **\n");
            printf("****************************************************************\n\n");
         }

         //check expid
         switch(TCBBoard.fexpid){
         case 0:
            printf("NO EXPERIMENT ENABLED\n\n");
         break;
         case 1:
            printf("Board compiled for MEG\n\n");
         break;
         case 2:
            printf("Board compiled for FOOT\n\n");
         break;
         case 4:
            printf("Board compiled for SCIFI\n\n");
         break;
         case 8:
            printf("Board compiled for LOLX\n\n");
         break;
         defaut:
            printf("expid = %d\n WARNING multiple experiment enabled while compiling the FW!\n\n", TCBBoard.fexpid);
         break;
         }

         printf(" RUNMODE status %x \n",data&0x1);
         printf(" INBUSY status %x \n",(data&0x2)>>1);
         printf(" FADCMODE status %x \n",(data&0x4)>>2);
         printf(" EXBUSY status %x \n",(data&0x8)>>3);
         printf(" ENABLE TRGBUS status %x \n",(data&0x10)>>4);
         printf(" TESTTXMODE status %x \n",(data&0x20)>>5);
         printf(" MASKFTRG status %x \n",(data&0x40)>>6);
         printf(" MASKFBUSY status %x \n",(data&0x80)>>7);
         printf(" DCBREADY status %x \n",(data&0x100)>>7);
         if( (TCBBoard.fidcode>>12)==2 || (TCBBoard.fidcode>>12)==1 ) 
            printf(" PATTERNSERDES status %x \n",(data&0x200)>>9);
         printf(" FBUSY status %x \n",(data&0x400)>>10);
         printf(" LOCAL_TRG status %x \n",(data&0x800)>>11);
         printf(" TRGBUS_NEGEDGE status %x \n",(data&0x1000)>>12);
         printf(" MASKBUSY status %x \n",(data&0x2000)>>13);
         printf(" MASKSYNC status %x \n",(data&0x4000)>>14);
         printf(" MASKTRG status %x \n",(data&0x8000)>>15);
         printf(" IDCODE status %x \n",(data&0xffff0000)>>16);
         printf(" RRUN reg content = %08x\n",data);

         if(((TCBBoard.fidcode&0xf000)>>12)==3) {
           int nword = (TCBBoard.fntrg-1)/32 + 1;
           for(int iword = 0; iword <nword; iword++){
             TCBBoard.GetRENA(&data,iword);
             printf(" TRGENA status %x, bit [%d:%d]\n",data,(iword+1)*32-1,iword*32);
           }
         }

         if(((TCBBoard.fidcode&0xf000)>>12)!=3) {
           TCBBoard.GetRALGSEL(&data);
           printf(" ALGSEL status %x \n",data);
         }

         if(TCBBoard.GetPacketizerBus()) 
            printf("WARNING: the local bus is used by the packetizer, some regs are not accessible!\n");

         //LED
         unsigned int led = TCBBoard.GetLed();

         printf("\n");
         printf("SYSBUSY | %c %c | SPARE\n", (led&0x20)?'O':' ', (led&0x10)?'O':' ');
         printf("PLLLOCK | %c %c | DTACK\n", (led&0x8)?'O':' ', (led&0x4)?'O':' ');
         printf("BUSY    | %c %c | RUN  \n", (led&0x2)?'O':' ', (led&0x1)?'O':' ');


      }
      //
      if(option == 3) {
         printf(" opt = 3 : Activate runmode ... \n");
         TCBBoard.GoRun();
      }
      //
      if(option == 4) {
         printf(" opt = 4 : Remove busy ... \n");
         TCBBoard.RemoveBusy();
      }
      //
      if(option == 5) {
         printf(" opt = 5 : SW stop ... \n");
         TCBBoard.SWStop();
      }
      //
      if(option == 6) {
         printf(" opt = 6 : SW sync ... \n");
         TCBBoard.SWSync();
      }
      //
      if(option == 7) {
         printf(" opt = 7 : Set precaling values (from presca.dat file) ... \n");
         filpresca = fopen("presca.dat","read");
         for(int irow = 0; irow<TCBBoard.fntrg; irow++) {
            fscanf(filpresca,"%x\n",presca+irow);
         }
         TCBBoard.SetPrescaling(presca);
      }
      //
      if(option == 8) {
      }
      if(option ==  9) {
         printf(" opt = 9 : Get TotalTime ... \n");
         TCBBoard.GetTotalTime(&data);
         printf("\n   Total Time latched by runmode for DAQ readout = %f sec\n",(float)data/1e6);
         TCBBoard.GetTotalTimeSPI(&data);
         printf("\n   Total Time free runnign through SPI= %f sec\n",(float)data/1e6);
      }
      if(option == 10) {
         printf(" opt = 10 : Get LiveTime ... \n");
         TCBBoard.GetLiveTime(&data);
         printf("\n   Live Time latched by runmode for DAQ readout = %f sec\n",(float)data/1e6);
         TCBBoard.GetLiveTimeSPI(&data);
         printf("\n   Total Time free runnign through SPI= %f sec\n",(float)data/1e6);

      }
      if(option == 11) {
         printf(" opt = 11 : Get Event Counter ... \n");
         TCBBoard.GetEventCounter(&data);
         printf("\n   Event Counter = %d\n",data);
         TCBBoard.GetSystemEventCounter(&data);
         printf("   System Event Counter = %d\n",data);
      }
      if(option == 12) {
         printf(" opt = 12 : Get trigger type ... \n");
         if((TCBBoard.fidcode >>12)==3){
            TCBBoard.GetTriggerType(&trgtype);
            printf("trigger type = %d\n",trgtype&0xFF);
            printf("readout enable = 0x%02x\n",trgtype>>26);
            int nword = (TCBBoard.fntrg-1)/32+1;
            for(int iword = 0; iword<nword; iword++) {
               TCBBoard.GetTriggerPattern(&tpattern,iword);
               printf("trigger trgpattern = %08x bit [%d:%d] \n",tpattern,(iword+1)*32-1,iword*32);
            }
         }
         u_int32_t trgreadoutenable;
         u_int32_t adcenable;
         if (TCBBoard.GetSystemTriggerType(&trgtype, &trgreadoutenable, &adcenable)){
            printf("system readout enable = 0x%02x\n", trgreadoutenable);
            printf("system adc read enable = 0x%01x\n", adcenable);
            printf("system trigger type = %d\n",trgtype);
         } else {
            printf("TRANSMISSION ERROR! reading = %d\n",trgtype);
         }

         u_int32_t errorcou;
         u_int32_t validcou;
         TCBBoard.GetTrgbusErrorCounter(&errorcou);
         TCBBoard.GetTrgbusValidCounter(&validcou);
         printf("%d errors\n", errorcou);
         printf("%d valid trgbus\n", validcou);
      }
      if(option == 13) {
         printf(" opt = 13 : Get Trigger Counters ... \n");
	 // registers for packetiser
	 TCBBoard.GetTriggerCounters(counters);
	 for(int icou = 0; icou<TCBBoard.fntrg; icou++)
	   printf("\n   Trigger Counter %d = %d\n",icou,counters[icou]);
	 // registers for SPI
	 u_int32_t temp[66];
	 TCBBoard.GetTriggerCountersSPI(counters);
	 for(int icou = 0; icou<TCBBoard.fntrg; icou++)
	   printf("\n   Trigger Counter through SPI %d = %d\n",icou,counters[icou]);
      }
      if(option == 14) {
         printf(" opt = 14 : Get Memory address ... \n");
         TCBBoard.GetMemoryAddress(&data);
         printf("\n   Memory address = %d\n",data);
      }
      //
      if(option == 15) {
        int crate,slot;
        char cratestring[256];
        printf(" opt = 15 : goto board, give crate number and slot ... \n");
        
        if(handle != -1){
           printf("Crate number?\n");
           scanf("%d",&crate);
           snprintf(cratestring,sizeof(cratestring),"mscb%d",crate);
           printf("Slot?\n");
           scanf("%d",&slot);

           //close current mscb connection and create a new one
           mscb_exit(handle);
           handle = mscb_init(cratestring, 0, "", 0);

           TCBBoard.SetMscbHandle(handle, slot);
        } else if(DCBBoard != nullptr){
           printf("DCB Name?\n");
           scanf("%s", cratestring);
           printf("Slot?\n");
           scanf("%d",&slot);

           //close current DCB connection and create a new one
           delete DCBBoard;
           std::string s(cratestring);
           DCBBoard = new DCB(s);
           DCBBoard->Connect();

           TCBBoard.SetDcbInterface(DCBBoard, slot);
         
        }

        TCBBoard.SetIDCode();
      }
      if(option == 16) {
         printf(" opt = 16 : Set trigger delay ... \n");
         u_int32_t data1,data2,data3,data4,data5;
         if((TCBBoard.GetIDCode()>>12)==0x3){
            //this is a Master
            printf("Trigger bus delay: SYNC output (0-1f in taps of 80ps)?(hex)\n");
            scanf("%x",&data);
            printf("Trigger bus delay: TRG output (0-1f in taps of 80ps)?(hex)\n");
            scanf("%x",&data1);
            printf("Trigger bus delay: SPARE output (0-1f in taps of 80ps)?(hex)\n");
            scanf("%x",&data2);
            TCBBoard.SetTRGBusODLY(&data,&data1,&data2);
         }
         printf("Trigger bus delay: SYNC input (0-1f in taps of 80ps)?(hex)\n");
         scanf("%x",&data);
         printf("Trigger bus delay: TRG input (0-1f in taps of 80ps)?(hex)\n");
         scanf("%x",&data1);
         printf("Trigger bus delay: SPARE input (0-1f in taps of 80ps)?(hex)\n");
         scanf("%x",&data2);
         TCBBoard.SetTRGBusIDLY(&data,&data1,&data2);
         TCBBoard.GetTRGBusDLY(&data,&data1,&data2,&data3,&data4,&data5);
         printf("I read back ODelay Sync = %x, ODelay Trg = %x, ODelay Spare = %x\n",data,data1,data2);
         printf("            IDelay Sync = %x, IDelay Trg = %x, IDelay Spare = %x\n",data3,data4,data5);
      }
      if(option == 17) {
         printf(" opt 17 = Write SERDES memory ... \n");
         int ichannel, imem, ifil;
         u_int32_t wdata[MEMDIM];
         printf(" serdes channel number? \n");
         scanf("%d",&ichannel);
         printf(" which memory? (0 = LSB, 1 = MSB) \n");
         scanf("%d",&imem);
         printf(" which file? (0 or 1) \n");
         scanf("%d",&ifil);
         if(ifil == 0)
            filin = fopen("writeserdesram.dat","read");
         else
            filin = fopen("writeserdesram2.dat","read");
         for(int irow = 0; irow<MEMDIM; irow++) {
            fscanf(filin,"%x\n",wdata+irow);
         }
         fclose(filin);
         TCBBoard.WriteSERDESMem(ichannel,imem,wdata);
      }
      if(option == 18) {
         printf(" opt 18 = Dump SERDES memory ... \n");
         int ichannel, imem;
         u_int32_t rdata[MEMDIM];
         printf(" serdes channel number? \n");
         scanf("%d",&ichannel);
         printf(" which memory? (0 = LSB, 1 = MSB) \n");
         scanf("%d",&imem);
         TCBBoard.ReadSERDESMem(ichannel,imem,rdata);
         filout = fopen("readserdesram.dat","write");
         for(int irow = 0; irow<MEMDIM; irow++) {
            fprintf(filout,"%08x\n",rdata[irow]);
         }
         fclose(filout);
      }
      if(option == 19) {
      }
      if(option == 20) {
      }
      if(option == 21) {
      }
      if(option == 22) {
      }
      if(option == 23) {
        printf(" opt = 23 : Set trigger mask ... \n");
        printf("Serdes Mask?(hex) ");
        scanf("%x",&data);
        TCBBoard.SetSerdesMask(&data);
      }
      if(option == 24) {
        u_int32_t offset;
        printf(" opt = 24 : Set Parameter ... \n");
        printf("Parameter offset? ");
        scanf("%d",&offset);
        printf("Value?(hex) ");
        scanf("%x",&data);
        TCBBoard.SetParameter(offset, &data);
      }
      if(option == 25) {
      }
      if(option == 26) {
      }
      if(option == 27) {
        int trgid;
        printf(" opt = 27 : Force a trigger ... \n");
        printf("Trigger Id? from 0 to %d \n",TCBBoard.fntrg-1);
        scanf("%d",&trgid);
        TCBBoard.ForceTrigger(trgid);
      }
      if(option == 28) {
        printf(" opt = 28 : Reset transmitter ... \n");
        TCBBoard.ResetTransmitter();
      }
      if(option == 29) {
      }
      if(option == 30) {
        printf(" opt = 30 : Dump Data ... \n");
        int ichannel;
        u_int32_t rdataA[MEMDIM];
        u_int32_t rdataB[MEMDIM];
        u_int32_t address;
        printf(" serdes channel number (0-15 input, 16 output)? \n");
        scanf("%d",&ichannel);
        TCBBoard.ReadSERDESMem(ichannel,0,rdataA);
        TCBBoard.ReadSERDESMem(ichannel,1,rdataB);
        TCBBoard.GetMemoryAddress(&address);
        filout = fopen("dataout.dat","write");
        fprintf(filout,"%04x %d %d %d\n", TCBBoard.fidcode, ichannel, address, MEMDIM);
        for(int irow = 0; irow<MEMDIM; irow++) {
           fprintf(filout,"%08x %08x\n",rdataB[irow], rdataA[irow]);
        }
        fclose(filout);
      }
      if(option == 31) {
        printf(" opt = 31 : Packetizer Commands \n");
        printf("\n  --- Options: \n");
        printf("[ 1]: Enable Packetizer    \t \t  [ 2]: Start Packetizer     \n");
        printf("[ 3]: Set Autostart        \t \t  [ 4]: Assign bus \n");  
        printf("[ 5]: Abort Packetizer     \t \t  [ 6]: Set Packetizer Command\n");
        printf("[ 7]: Load Program         \t \t  [ 8]: Enable ReadoutFSM\n");
        int sel;
        do {
          printf("Give an option: ");
          scanf("%s",opline);
          sel = strtod(opline,NULL);
        } while ( sel == 0 ) ;
        switch(sel){
          case 1: 
            printf("enable [0/1]? ");
            int ans;
            scanf("%d", &ans);
            TCBBoard.SetPacketizerEnable(ans == 1);
            break;
          case 2: TCBBoard.StartPacketizer(); break;
          case 3:
            printf("enable [0/1]? ");
            scanf("%d", &ans);
            TCBBoard.SetPacketizerAutostart(ans == 1);
            break;
          case 4: 
            printf("assign bus to packetizer [0/1]? ");
            scanf("%d", &ans);
            TCBBoard.SetPacketizerBus(ans == 1);
            break;
          case 5: TCBBoard.AbortPacketizer(); break;
          case 6: 
            int offset, cmd;
            u_int32_t arg0, arg1, opt;
            printf("offset (0-1024) ");
            scanf("%d", &offset);
            printf("command name (0-stop, 1-copy, 2-block_copy, 3-write, 4-jump, 5-jumpIf) ");
            scanf("%d", &cmd);
            printf("argument 0, \"source\" (hex) ");
            scanf("%x", &arg0);
            printf("argument 1, \"destination\" (hex) ");
            scanf("%x", &arg1);
            printf("option (hex) ");
            scanf("%x", &opt);
            TCBBoard.SetPacketizerCommandAt(offset, (PACKETIZER_COMMAND)cmd, arg0, arg1, opt);
            break;
          case 7:
            {
            char filenameIn[256];
            FILE *fileIn;
            printf("Filename: ");
            scanf("%s", filenameIn);
            fileIn=fopen(filenameIn, "r");
            std::vector<PacketInstruction> instVec;
            while(!feof(fileIn)){
               int lineno=0;
               char cmd[16];
               unsigned int arg0=0;
               unsigned int arg1=0;
               unsigned int arg2=0;
               char arg0str[16];
               char arg1str[16];
               PacketInstruction inst;
               fscanf(fileIn, "%d: %[^ \n]%*[ \n]",  &lineno, cmd);
               inst.offset = lineno;
               if(strcmp(cmd, "STOP")==0){
                  inst.cmd = ::STOP;
               } else if(strcmp(cmd, "JUMP")==0){
                  inst.cmd = ::JUMP;
                  fscanf(fileIn, " %d%*[ \n]", &arg2);
               } else if(strcmp(cmd, "COPY")==0){
                  inst.cmd = ::COPY;
                  fscanf(fileIn, " %s %s%*[ \n]", arg0str, arg1str);
                  str2reg(arg0str, &arg0);
                  str2reg(arg1str, &arg1);
               } else if(strcmp(cmd, "BLOCK_COPY")==0){
                  inst.cmd = ::BLOCK_COPY;
                  fscanf(fileIn, " %s %s %d%*[ \n]", arg0str, arg1str, &arg2);
                  str2reg(arg0str, &arg0);
                  str2reg(arg1str, &arg1);
               } else if(strcmp(cmd, "W")==0){
                  inst.cmd = ::DIRECT_WRITE;
                  fscanf(fileIn, " %s %s%*[ \n]", arg0str, arg1str);
                  str2reg(arg0str, &arg0);
                  str2reg(arg1str, &arg1);
               } else if(strcmp(cmd, "JIF")==0){
                  inst.cmd = ::JUMP_IF;
                  fscanf(fileIn, " %s %s %d\n", arg0str, arg1str, &arg2);
                  str2reg(arg0str, &arg0);
                  str2reg(arg1str, &arg1);
               }
               inst.arg0 = arg0;
               inst.arg1 = arg1;
               inst.arg2 = arg2;

               instVec.push_back(inst);
            }
            TCBBoard.WritePacketizerProgram(instVec);
            fclose(fileIn);
            }
            break; 
          case 8:
            printf("enable [0/1]? ");
            scanf("%d", &ans);
            TCBBoard.SetReadoutEnable(ans == 1);
            break;
          default: break;
        }
      }
      if(option == 32) {
        printf(" opt = 32 : Buffer Commands \n");
        printf("[ 1]: Dump Buffer    \t \t  [ 2]: Increment Pointer\n");
        printf("[ 3]: Reset Logic    \t \t  [ 4]: Get State\n");
        printf("[ 5]: Read Banks     \t \t  [  ]:          \n");
        int sel;
        do {
          printf("Give an option: ");
          scanf("%s",opline);
          sel = strtod(opline,NULL);
        } while ( sel == 0 ) ;
        switch(sel){
          case 1: 
            u_int32_t buf[BUFFERSIZE];
            TCBBoard.ReadBuffer(buf);
            for(int i=0; i<BUFFERSIZE; i++) printf("%08x\n", buf[i]);
            break; 
          case 2:
            TCBBoard.IncrementBufferPointer();
            break;
          case 3:
            TCBBoard.ResetBufferLogic();
            break;
          case 4:
            printf ("SPI pointer: %d, Packetizer pointer %d, Memory State %x", TCBBoard.GetSPIBufferPointer(), TCBBoard.GetPacketizerBufferPointer(), TCBBoard.GetBufferState());
            break;
          case 5:
            {
            int nBanks=0;
            char bankName[4];
            int length;
            u_int32_t ptr= TCBBoard.GetBufferHeadSPI(&nBanks);
            printf("packager with %d banks\n", nBanks);
            while(TCBBoard.HasBufferBankSPI(ptr, bankName, &length)){
               printf("Got bank %c %c %c %c\n", bankName[3], bankName[2], bankName[1], bankName[0]);
               u_int32_t *data = new u_int32_t[length];
               printf("Data:\n");
               TCBBoard.GetBufferBankDataSPI(ptr, data, length);
               for(int i=0; i<length; i++ ) printf("%3d: %08x\n", i, data[i]);
               delete[] data;
               ptr = TCBBoard.SkipBufferBankSPI(ptr, length);
            }
            TCBBoard.IncrementBufferPointer();
            }
            break;
          default: break;
        }
      }
      //reset PLL
      if(option == 33) {
        printf(" opt = 33 : PLL reset ... \n");
        TCBBoard.ResetPLLs();
      }
      // reset PLL unlock counter
      if(option == 34) {
        printf(" opt = 34 : Reset PLL unlock counter ... \n");
        TCBBoard.ResetPLLUnlockCou();
      }
      // get PLL unlock counter
      if(option == 35) {
        u_int32_t rpcou;
        printf(" opt = 35 : read PLL unlock counter ... \n");
        TCBBoard.GetPLLUnlockCou(&rpcou);
        printf(" PLL unlock counter value: %d\n", rpcou);
      }
      //
      if(option == 36) {
        printf(" opt = 36 : Get precaling values (from presca.dat file) ... \n");
        TCBBoard.GetPrescaling(presca);
        for(int irow = 0; irow<TCBBoard.fntrg; irow++) printf("trg%d: %x\n", irow, presca[irow]);
      }
      //
      if(option == 37) {
        printf(" opt = 37 : Set trigger delay values (from trgdly.dat file) ... \n");
        filtrgdly = fopen("trgdly.dat", "read");
        for(int irow = 0; irow<TCBBoard.fntrg; irow++) {
           fscanf(filtrgdly,"%x\n",trgdly+irow);
           if(trgdly[irow]>0){
               trgdly_enable[irow] = true;
               trgdly[irow] -= 1;
           } else {
               trgdly_enable[irow] = false;
           }
        }
        TCBBoard.SetTRGDLY(trgdly_enable, trgdly);
      }
      //
      if(option == 38) {
        printf(" opt = 38 : Get trigger delay values (from trgdly.dat file) ... \n");
        TCBBoard.GetTRGDLY(trgdly);
        for(int irow = 0; irow<TCBBoard.fntrg; irow++) printf("trg%d: %x\n", irow, trgdly[irow]);
      }
      //
      if(option == 39) {
        printf(" opt = 39 : start automatic calibration ... \n");
        TCBBoard.AutoCalibrateSerdes();
      }
      //
      if(option == 40) {
        printf(" opt = 40 : Current serdes status ... \n");
        u_int32_t dly[2*16];
        int bit[8*16];
        TCBBoard.ReadCurrentSerdes(dly, bit);
        for(int i=0; i<TCBBoard.fnserdes*8; i++)
           printf("serdes %d link %d: dly %x bitslip %d\n", i/8, i%8, (dly[i/4]>>((i%4)*8))&0x1f, bit[i]);
      }
      //
      if(option == 41) {
        printf(" opt = 41 : Current serdes autolock status ... \n");
        u_int32_t state;
        TCBBoard.GetAutoCalibrateFail(&state);
        printf("fail: %x\n", state);
        TCBBoard.GetAutoCalibrateBusy(&state);
        printf("busy: %x\n", state);
      }
      //
      if(option == 42) {
        printf(" opt = 42 : Performing dummy calibration to draw serdes eyes ... \n");
        u_int32_t eyes[16];
        TCBBoard.GetAutoCalibrateEye(eyes);
        for(int i=0; i<16; i++){
          printf("serdes %d: ", i);
          for(int j=0; j<32; j++){
            if(eyes[i] & (1<<j)){
              printf("# ");
            } else {
              printf("- ");
            }
          }
          printf("\n");
        }
      }
      if(option == 43) {
        printf(" opt = 43 : Single crate trigger configuration ...\n");
        int choice;
        printf("Select trigger conf (AND=0, OR=1): ");
        scanf("%d", &choice);
   short shaper;
        printf("Select shaper time (0-32 clks): ");
        scanf("%hd", &shaper);
        char buf[300];
        printf("Channels to be configured (comma separated): ");
   scanf("%s", buf);

   char * p = strtok (buf,",");
        std::vector<int> a;
        while (p != NULL){
          int ch = atoi(p);
          p = strtok (NULL, ",");
          a.push_back(ch);
        }

        if(choice==0){
          TCBBoard.SetSingleCrateTriggerAnd(a.size(), &a[0], shaper);
        } else {
          TCBBoard.SetSingleCrateTriggerOr(a.size(), &a[0], shaper);
        }
      }
      if(option == 44) {
         printf(" opt = 44 : Read precaling values ... \n");
	 u_int32_t pvalue[64];
	 TCBBoard.GetPrescaling(pvalue);
         for(int irow = 0; irow<TCBBoard.fntrg; irow++) {
	   printf("prescaling for trigger %d is %d\n",irow,pvalue[irow]);
         }
      }
      if(option == 45) {
         printf(" opt = 45 : Perform a SCIFI run ... \n");
	 // file name
	 printf("type the file name\n");
	 char fname[100];
	 scanf("%s", fname);
	 // how many runs?
	 printf("type how many runs\n");
	 int nrun;
	 scanf("%d", &nrun);
	 // how long in sec?
	 printf("type run un duration in sec\n");
	 int rsec;
	 scanf("%d", &rsec);
	 // loop on runs
	 for(int irun = 0; irun<nrun; irun++) {
	   // open the file
	   FILE *ffile;
	   char filename[100];
	   snprintf(filename,sizeof(filename),"%s-%d.dat", fname, irun);
      printf("starting run to file %s\n", filename);
	   ffile = fopen(filename,"w");
	   // disable all trigger
      u_int32_t rena = 0;
	   TCBBoard.SetRENA(&rena,0);
	   TCBBoard.SetRENA(&rena,1);
	   // give a sync
	   TCBBoard.SWSync();
	   // put in runmode
	   TCBBoard.GoRun();
	   // wait n seconds
	   sleep(rsec-1);
	   // put in stop
	   TCBBoard.SWStop();
	   // read counter values + total time
	   u_int32_t data[44];
	   TCBBoard.ReadBLT(0x700,data,43);
	   // read total time 
	   TCBBoard.GetTotalTime(data+43);
	   //write to file
	   for(int iword = 0; iword<44; iword++)
	     fprintf(ffile,"%d\n",data[iword]);
	   //close the file
	   fclose(ffile);
	   printf(" Run %d DONE!\n", irun);
	 }// end loop on runs
      }
      if(option == 46) {
         printf(" opt = 46 : read a register ... \n");
          printf("\nAddress?(hex)\n");
          scanf("%x",&scanfdata);
          TCBBoard.ReadReg(scanfdata, &data);
          printf("[%4x]: %08x\n",scanfdata, data);
	 
      }
      if(option == 47) {
         printf(" opt = 47 : write a register ... \n");
          printf("Address?(hex)\n");
          scanf("%x",&scanfdata);
          printf("value?(hex)\n");
          scanf("%x",&data);
          TCBBoard.WriteReg(scanfdata, &data);
          TCBBoard.ReadReg(scanfdata, &data);
          printf("[%4x]: %08x\n",scanfdata, data);
	 
      }

      

      /* end of the main loop on the options*/
   } while ( option >= 0);
   
   
   /* normal exit: close the VME crate */
   printf(" exiting ... \n");

   //
   // close mscb connection
   if(handle != -1) mscb_exit(handle);
   if(DCBBoard != nullptr) delete DCBBoard;
   return 0;
}

